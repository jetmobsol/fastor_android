This project is based on https://github.com/PatilShreyas/Foodium

But was improved with 2 bug fixes and a lot of modifications,
one of them I think most important from architecture point of view.
Please take into account when You will change locale in options menu,
that not all restaurants have all 3 localization translations.
So if translation not changed, it is not a bug in app.

Below listed changes:

1) One of HUGE architecture bug is that One Model used in all layers.

1 Object which is Room entity object, was used in each layer which is not correct,
according to separation of concerns principe.
So I decided to fix it:
1. GetPagedProductsResponse for external API
2. DbProduct for local Room entity
3. ProductDomainModel for using in Presentation layer

2) Solved Bug connected with swipe down to refresh, not correct Flow implementation was used

3) Have used my personal API from Fastor E-commerce project
It was needed to change a little bit in API for this project
Now by using this API is possible to get first 100 products
http://cms.fastorbot.com/api/products/1/100/all
Where 1 is page number
      100 is page size
      all - it is to show from all companies, can be companyId or "all"

If to plan ROAD MAP for the project, next features can be done:
1) paging
2) possible to choose restaurant, instead of all will be companyId

4) Completely changed package structure according to modularized code-base approach
with many benefits like:
 1. better separation of concerns.
 2. features can be developed in parallel eg. by different teams
 3. each feature can be developed in isolation, independently from other features


5) Refactored Repository pattern to make it testable
 in domain level added ProductsRepository interface
 and implementation ProductsRepositoryIml in data level


6) Inserted in domain level UseCases pattern
Good explanation can be found by link
https://proandroiddev.com/why-you-need-use-cases-interactors-142e8a6fe576

7) Added Stetho library for HTTP and DB debugging
