package com.jms.fastor.app.di.component

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import com.jms.fastor.app.FastorApp
import com.jms.fastor.app.di.module.*
import com.jms.fastor.feature.product.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        FastorDatabaseModule::class,
        ViewModelFactoryModule::class,

        ProductsActivityBuilder::class,
        ProductsApiModule::class,
        ProductsDaoModule::class,
        ProductsViewModelModule::class,
        ProductsRepositoryModule::class
    ]
)
interface AppComponent : AndroidInjector<FastorApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun create(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: FastorApp)
}