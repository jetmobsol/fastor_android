package com.jms.fastor.app.di.module

import android.app.Application
import com.jms.fastor.feature.product.data.local.FastorProductsDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FastorDatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(application: Application) = FastorProductsDatabase.getInstance(application)

}