package com.jms.fastor.feature.product.presentation.products.adapter

import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.jms.fastor.R
import com.jms.fastor.databinding.ProductsItemBinding
import com.jms.fastor.feature.product.domain.model.ProductDomainModel

/**
 * [RecyclerView.ViewHolder] implementation to inflate View for RecyclerView.
 * See [ProductListAdapter]]
 */
class ProductViewHolder(private val binding: ProductsItemBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(product: ProductDomainModel, onItemClickListener: ProductListAdapter.OnItemClickListener? = null) {
        binding.productName.text = product.productName
        binding.companyName.text = product.companyName
        binding.price.text = product.productPrice
        binding.imageView.load(product.imageURL) {
            placeholder(R.drawable.ic_photo)
            error(R.drawable.ic_broken_image)
        }

        onItemClickListener?.let { listener ->
            binding.root.setOnClickListener {
                listener.onItemClicked(product, binding.imageView)
            }
        }
    }
}
