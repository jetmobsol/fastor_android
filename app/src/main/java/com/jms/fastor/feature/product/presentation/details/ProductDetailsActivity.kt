package com.jms.fastor.feature.product.presentation.details

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import coil.api.load
import com.jms.fastor.databinding.ProductDetailsActivityBinding
import com.jms.fastor.app.presentation.BaseActivity
import com.jms.fastor.app.utils.viewModelOf
import kotlinx.android.synthetic.main.activity_post_details.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class ProductDetailsActivity : BaseActivity<ProductDetailsViewModel, ProductDetailsActivityBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true);
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val productId = intent.extras?.getString(PRODUCT_ID)
            ?: throw IllegalArgumentException("postId must be non-null")
        mViewBinding.collapsingToolbar.title = "Product ID = ${productId}"
        initProduct(productId)
    }

    private fun initProduct(productId: String) {
        mViewModel.getProduct(productId).observe(this, Observer { product ->


            mViewBinding.productContent.apply {
                companyName.text = product.companyName

                product.names?.firstOrNull { it.locale?.toLowerCase() == "lv" }?.apply {
                    productLv.visibility = View.VISIBLE
                    productNameLv.text = this.name
                    productContentLv.text = this.content
                }

                product.names?.firstOrNull { it.locale?.toLowerCase() == "ru" }?.apply {
                    productRu.visibility = View.VISIBLE
                    productNameRu.text = this.name
                    productContentRu.text = this.content
                }

                product.names?.firstOrNull { it.locale?.toLowerCase() == "en" }?.apply {
                    productEn.visibility = View.VISIBLE
                    productNameEn.text = this.name
                    productContentEn.text = this.content
                }
                val sb = StringBuilder()
                product.prices?.forEach {
                    sb.append(" ${it.price}  ${if(!it.priceDesc.isNullOrBlank())" - ${it.priceDesc}" else ""}    ")
                }
                productPrice.text = sb.toString().trim()
            }
            mViewBinding.imageView.load(product.imageURL)
        })
    }

    companion object {
        const val PRODUCT_ID = "productId"
    }

    override fun getViewBinding(): ProductDetailsActivityBinding =
        ProductDetailsActivityBinding.inflate(layoutInflater)

    override fun getViewModel() = viewModelOf<ProductDetailsViewModel>(mViewModelProvider)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                supportFinishAfterTransition()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
