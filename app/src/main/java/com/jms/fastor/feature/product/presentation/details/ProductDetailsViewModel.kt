package com.jms.fastor.feature.product.presentation.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.jms.fastor.feature.product.domain.usecases.GetProductUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

/**
 * ViewModel for [ProductDetailsActivity]
 */
@ExperimentalCoroutinesApi
class ProductDetailsViewModel @Inject constructor(private val getProductUseCase: GetProductUseCase) :
    ViewModel() {

    fun getProduct(id: String) = getProductUseCase.getProductById(id).asLiveData()
}
