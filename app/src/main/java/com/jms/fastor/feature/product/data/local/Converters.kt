package com.jms.fastor.feature.product.data.local

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.Types.newParameterizedType

class Converters {

    @TypeConverter
    fun fromJsonToProductName(value: String): List<ProductName>? {
        return fromJsonToObject(value)
    }

    @TypeConverter
    fun fromProductNameToJson(list: List<ProductName>? ): String? {
        return fromObjectToJson(list)
    }

    @TypeConverter
    fun fromJsonToProductPrice(value: String): List<ProductPrice>? {
        return fromJsonToObject(value)
    }

    @TypeConverter
    fun fromProductPriceToJson(list: List<ProductPrice>? ): String? {
        return fromObjectToJson(list)
    }


    inline fun <reified T> fromObjectToJson(list: List<T>? ): String? {
        val moshi = Moshi.Builder().build()
        val type = Types.newParameterizedType(List::class.java, T::class.java)
        val adapter = moshi.adapter<List<T>>(type)
        return adapter.toJson(list)
    }


    inline fun <reified T> fromJsonToObject(value: String): List<T>? {
        val moshi = Moshi.Builder().build()
        val type = newParameterizedType(List::class.java, T::class.java)
        val adapter = moshi.adapter<List<T>>(type)
        return adapter.fromJson(value)
    }





}