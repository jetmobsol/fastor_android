package com.jms.fastor.feature.product

import dagger.Module
import dagger.android.ContributesAndroidInjector
import com.jms.fastor.feature.product.presentation.details.ProductDetailsActivity
import com.jms.fastor.feature.product.presentation.products.ProductsActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi

@Module
abstract class ProductsActivityBuilder {

    @ExperimentalCoroutinesApi
    @ContributesAndroidInjector
    abstract fun bindProductsActivity(): ProductsActivity

    @ExperimentalCoroutinesApi
    @ContributesAndroidInjector
    abstract fun bindProductDetailsActivity(): ProductDetailsActivity

}
