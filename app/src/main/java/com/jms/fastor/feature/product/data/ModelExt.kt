package com.jms.fastor.feature.product.data

import com.jms.fastor.feature.product.data.local.DbProduct
import com.jms.fastor.feature.product.data.local.ProductName
import com.jms.fastor.feature.product.data.local.ProductPrice
import com.jms.fastor.feature.product.data.remote.api.FastorCmsSerevice
import com.jms.fastor.feature.product.data.remote.response.GetPagedProductsResponse
import com.jms.fastor.feature.product.domain.model.CompleteProductDomainModel
import com.jms.fastor.feature.product.domain.model.ProductDomainModel
import com.jms.fastor.feature.product.domain.model.ProductNameDomainModel
import com.jms.fastor.feature.product.domain.model.ProductPriceDomainModel

fun GetPagedProductsResponse.toDbProductList(): List<DbProduct> {
    return this.values?.map { it ->
        DbProduct(
            id = it.id,
            companyName = it.companyName,
            imageURL = it.imageURL,
            names = it.nameList?.map { prodResp ->
                ProductName(
                    prodResp.locale,
                    prodResp.name,
                    prodResp.content
                )
            },
            prices = it.priceListCms?.map { prodPrice ->
                ProductPrice(
                    prodPrice.id,
                    prodPrice.price,
                    prodPrice.priceDesc,
                    prodPrice.order
                )
            }
        )
    }?.toList() ?: listOf()
}

fun List<DbProduct>.toProductDomainModelList(locale: String) =
    this.map { it.toProductDomainModel(locale) }

fun DbProduct.toProductDomainModel(locale: String?):ProductDomainModel {

    val foundLocale = this.names?.filter { it.locale?.toLowerCase() == locale?.toLowerCase() }

    val productName =
        when{
            foundLocale?.isNotEmpty()==true -> {
                foundLocale.first().name
            }
            this.names?.isNotEmpty()==true -> {
                this.names!!.first().name
            }
            else ->"NOT FOUND NAME!!!"
        }

    val price = this.prices?.first()?.price?.trim()

    return ProductDomainModel(
        id = this.id,
        companyName = this.companyName,
        productName = productName,
        productPrice =this.prices?.first()?.price?.trim(),
        imageURL = FastorCmsSerevice.FASTOR_CMS_API_URL + this.imageURL
    )
}

fun DbProduct.toCompleteProductDomainModel(): CompleteProductDomainModel {
    return CompleteProductDomainModel(
        id = this.id,
        companyName = this.companyName,
        imageURL = FastorCmsSerevice.FASTOR_CMS_API_URL + this.imageURL,
        names =this.names?.map { ProductNameDomainModel(it.locale, it.name, it.content)},
        prices =this.prices?.map { ProductPriceDomainModel(it.id,it.price, it.priceDesc, it.order) }
    )
}

