package com.jms.fastor.feature.product.domain.model


data class ProductDomainModel(
    val id: String,
    val companyName: String? = null,
    val productName: String? = null,
    val productPrice: String? = null,
    val imageURL: String? = null
)

data class CompleteProductDomainModel(
    var id: String,
    var companyName: String? = null,
    var imageURL: String? = null,
    var names: List<ProductNameDomainModel>? = null,
    var prices: List<ProductPriceDomainModel>? = null

)

data class ProductNameDomainModel(
    var locale: String?=null,
    val name: String,
    val content: String? = null
)

data class ProductPriceDomainModel(
    val id: String,
    val price: String,
    val priceDesc: String?=null,
    val order: Int
)
