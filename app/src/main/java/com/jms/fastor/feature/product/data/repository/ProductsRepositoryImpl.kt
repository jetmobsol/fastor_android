package com.jms.fastor.feature.product.data.repository

import androidx.annotation.MainThread
import com.jms.fastor.feature.product.data.local.DbProduct
import com.jms.fastor.feature.product.data.local.dao.ProductsDao
import com.jms.fastor.feature.product.data.remote.api.FastorCmsSerevice
import com.jms.fastor.feature.product.data.remote.response.GetPagedProductsResponse
import com.jms.fastor.feature.product.data.toDbProductList
import com.jms.fastor.feature.product.data.toProductDomainModelList
import com.jms.fastor.feature.product.domain.model.ProductDomainModel
import com.jms.fastor.app.utils.State
import com.jms.fastor.feature.product.data.toCompleteProductDomainModel
import com.jms.fastor.feature.product.domain.model.CompleteProductDomainModel
import com.jms.fastor.feature.product.domain.repository.ProductsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import retrofit2.Response
import javax.inject.Inject

/**
 * Singleton repository for fetching data from remote and storing it in database
 * for offline capability. This is Single source of data.
 */
@ExperimentalCoroutinesApi
class ProductsRepositoryImpl @Inject constructor(
    private val productsDao: ProductsDao,
    private val fastorCmsSerevice: FastorCmsSerevice
) : ProductsRepository {

    /**
     * Fetched the Products from network and stored it in database. At the end, data from persistence
     * storage is fetched and emitted.
     */
    override fun getAllProducts(locale: String): Flow<State<List<ProductDomainModel>>> {
        return object : NetworkBoundRepository<List<DbProduct>, GetPagedProductsResponse>() {

            override fun fetchFromLocal(): Flow<List<DbProduct>> = productsDao.getAllProducts()

            override suspend fun fetchFromRemote(): Response<GetPagedProductsResponse> =
                fastorCmsSerevice.getProducts()


            override suspend fun saveRemoteData(response: GetPagedProductsResponse) {
                response.toDbProductList()
                    .let { if (it.isNotEmpty()) productsDao.insertProducts(it) }
            }

        }.asFlow()
            .domainMap(locale)
            .flowOn(Dispatchers.IO)
    }


    override fun getAllProductsFromLocalDb(locale: String): Flow<State<List<ProductDomainModel>>> {
        return flow<State<List<DbProduct>>> {
            emit(State.success(productsDao.getAllProducts().first()))
        }
            .domainMap(locale)
            .flowOn(Dispatchers.IO)
    }

    /**
     * Retrieves a Product with specified [ProductId].
     * @param ProductId Unique id of a [Product].
     * @return [Product] data fetched from the database.
     */
    @MainThread
    override fun getProductById(productId: String): Flow<CompleteProductDomainModel> {
        return productsDao.getProductById(productId).map { it.toCompleteProductDomainModel() }
    }

    fun Flow<State<List<DbProduct>>>.domainMap(locale: String): Flow<State<List<ProductDomainModel>>> {
        return this.map { dbProductState ->
            when (dbProductState) {
                is State.Success -> {
                    State.success(dbProductState.data.toProductDomainModelList(locale))
                }
                is State.Error -> {
                    State.error<List<ProductDomainModel>>(dbProductState.message)
                }
                is State.Loading -> {
                    State.loading()
                }
            }
        }
    }

}
