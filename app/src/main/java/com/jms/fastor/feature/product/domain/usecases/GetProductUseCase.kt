package com.jms.fastor.feature.product.domain.usecases

import com.jms.fastor.feature.product.domain.repository.ProductsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Singleton
class GetProductUseCase @Inject constructor(
    private val productsRepository: ProductsRepository
) {
    /**
     * Retrieves a Product with specified [ProductId].
     * @param ProductId Unique id of a [Product].
     * @return [Product] data fetched from the database.
     */
    fun getProductById(productId: String) = productsRepository.getProductById(productId)

}
