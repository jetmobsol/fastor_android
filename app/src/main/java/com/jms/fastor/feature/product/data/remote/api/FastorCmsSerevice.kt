package com.jms.fastor.feature.product.data.remote.api

import com.jms.fastor.feature.product.data.remote.response.GetPagedProductsResponse
import retrofit2.Response
import retrofit2.http.GET

interface FastorCmsSerevice {

    @GET("/api/products/1/100/all")
    suspend fun getProducts(): Response<GetPagedProductsResponse>

    companion object {
        const val FASTOR_CMS_API_URL = "http://cms.fastorbot.com"
    }
}