package com.jms.fastor.feature.product.domain.repository

import com.jms.fastor.app.utils.State
import com.jms.fastor.feature.product.domain.model.CompleteProductDomainModel
import com.jms.fastor.feature.product.domain.model.ProductDomainModel
import kotlinx.coroutines.flow.Flow

interface ProductsRepository {

    fun getAllProducts(locale:String): Flow<State<List<ProductDomainModel>>>

    fun getProductById(productId: String): Flow<CompleteProductDomainModel>

    fun getAllProductsFromLocalDb(locale:String): Flow<State<List<ProductDomainModel>>>

}
