package com.jms.fastor.feature.product

import com.jms.fastor.feature.product.data.local.FastorProductsDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ProductsDaoModule {

    @Singleton
    @Provides
    fun provideProductsDao(database: FastorProductsDatabase) = database.getProductsDao()
}