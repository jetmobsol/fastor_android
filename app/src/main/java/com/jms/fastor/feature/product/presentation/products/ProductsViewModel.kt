package com.jms.fastor.feature.product.presentation.products

import androidx.lifecycle.*
import com.jms.fastor.feature.product.domain.model.ProductDomainModel

import com.jms.fastor.app.utils.State
import com.jms.fastor.feature.product.domain.usecases.GetAllProductsFromLocalDbUseCase
import com.jms.fastor.feature.product.domain.usecases.GetAllProductsUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel for [ProductsActivity]
 */
@ExperimentalCoroutinesApi
class ProductsViewModel @Inject constructor(
    private val getAllProductsUseCase: GetAllProductsUseCase,
    private val getAllProductsFromLocalDbUseCase: GetAllProductsFromLocalDbUseCase
    ) :
    ViewModel() {
    private val _localeLiveData = MutableLiveData<String>("lv")
    private val _productsLiveData = MutableLiveData<State<List<ProductDomainModel>>>()

    val productsLiveData: LiveData<State<List<ProductDomainModel>>>
        get() = _productsLiveData



    fun getProducts() {
        viewModelScope.launch {
            getAllProductsUseCase.getAllProducts(_localeLiveData.value.toString()).collect {
                _productsLiveData.value = it
            }
        }
    }

    fun setLocale(newLocale:String){
        viewModelScope.launch {
            _localeLiveData.value = newLocale
            getAllProductsFromLocalDbUseCase.getAllProductsFromLocalDb(_localeLiveData.value.toString()).collect {
                _productsLiveData.value = it
            }
        }
    }

}
