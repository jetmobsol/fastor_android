package com.jms.fastor.feature.product.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jms.fastor.feature.product.data.local.dao.ProductsDao

/**
 * Abstract Fastor database.
 * It provides DAO [ProductsDao] by using method [getProductsDao].
 */
@Database(
    entities = [DbProduct::class],
    version = DatabaseMigrations.DB_VERSION
)
@TypeConverters(Converters::class)
abstract class FastorProductsDatabase : RoomDatabase() {

    /**
     * @return [ProductsDao] Fastor Products Posts Data Access Object.
     */
    abstract fun getProductsDao(): ProductsDao

    companion object {
        const val DB_NAME = "fastor_database"

        @Volatile
        private var INSTANCE: FastorProductsDatabase? = null

        fun getInstance(context: Context): FastorProductsDatabase {
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }

            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        FastorProductsDatabase::class.java,
                    DB_NAME
                    )
                    .addMigrations(*DatabaseMigrations.MIGRATIONS)
                    .build()

                INSTANCE = instance
                return instance
            }
        }

    }
}