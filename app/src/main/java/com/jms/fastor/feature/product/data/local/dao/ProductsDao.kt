package com.jms.fastor.feature.product.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jms.fastor.feature.product.data.local.DbProduct
import kotlinx.coroutines.flow.Flow

/**
 * Data Access Object (DAO) for [com.jms.fastor.data.local.FastorProductsDatabase]
 */
@Dao
interface ProductsDao {

    /**
     * Inserts [Products] into the [DbProduct.TABLE_NAME] table.
     * Duplicate values are replaced in the table.
     * @param Products Products
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProducts(Products: List<DbProduct>)

    /**
     * Deletes all the Products from the [DbProduct.TABLE_NAME] table.
     */
    @Query("DELETE FROM ${DbProduct.TABLE_NAME}")
    fun deleteAllProducts()

    /**
     * Fetches the Product from the [DbProduct.TABLE_NAME] table whose id is [productId].
     * @param productId Unique ID of [DbProduct]
     * @return [Flow] of [DbProduct] from database table.
     */
    @Query("SELECT * FROM ${DbProduct.TABLE_NAME} WHERE ID = :productId")
    fun getProductById(productId: String): Flow<DbProduct>

    /**
     * Fetches all the Products from the [Product.TABLE_NAME] table.
     * @return [Flow]
     */
    @Query("SELECT * FROM ${DbProduct.TABLE_NAME}")
    fun getAllProducts(): Flow<List<DbProduct>>


    /*@Query("SELECT * FROM ${DbProduct.TABLE_NAME}")
    fun getAllProductsSimple(): List<DbProduct>*/

}