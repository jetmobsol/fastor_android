package com.jms.fastor.feature.product.domain.usecases

import com.jms.fastor.feature.product.domain.repository.ProductsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Singleton
class GetAllProductsUseCase @Inject constructor(
    private val productsRepository: ProductsRepository
) {
    /**
     * Fetched the posts from network and stored it in database. At the end, data from persistence
     * storage is fetched and emitted.
     */
    fun getAllProducts(locale:String) = productsRepository.getAllProducts(locale)

}
