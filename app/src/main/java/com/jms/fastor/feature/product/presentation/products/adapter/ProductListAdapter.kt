package com.jms.fastor.feature.product.presentation.products.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.jms.fastor.databinding.ProductsItemBinding
import com.jms.fastor.feature.product.domain.model.ProductDomainModel

/**
 * Adapter class [RecyclerView.Adapter] for [RecyclerView] which binds [Product] along with [ProductViewHolder]
 * @param onItemClickListener Listener which will receive callback when item is clicked.
 */
class ProductListAdapter(private val onItemClickListener: OnItemClickListener) :
    ListAdapter<ProductDomainModel, ProductViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProductViewHolder(
        ProductsItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) =
        holder.bind(getItem(position), onItemClickListener)

    interface OnItemClickListener {
        fun onItemClicked(product: ProductDomainModel, imageView: ImageView)
    }

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ProductDomainModel>() {
            override fun areItemsTheSame(oldItem: ProductDomainModel, newItem: ProductDomainModel): Boolean =
                oldItem.id == newItem.id/* && oldItem.productName == newItem.productName*/

            override fun areContentsTheSame(oldItem: ProductDomainModel, newItem: ProductDomainModel): Boolean =
                oldItem == newItem

        }
    }
}
