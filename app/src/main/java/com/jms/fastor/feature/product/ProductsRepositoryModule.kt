package com.jms.fastor.feature.product

import dagger.Module
import dagger.Provides
import com.jms.fastor.feature.product.data.local.dao.ProductsDao
import com.jms.fastor.feature.product.data.remote.api.FastorCmsSerevice
import com.jms.fastor.feature.product.data.repository.ProductsRepositoryImpl
import com.jms.fastor.feature.product.domain.repository.ProductsRepository
import javax.inject.Singleton

@Module
class ProductsRepositoryModule {

    @Singleton
    @Provides
    fun provideProductsRepository(productsDao: ProductsDao, fastorCmsSerevice: FastorCmsSerevice): ProductsRepository {
        return  ProductsRepositoryImpl (productsDao, fastorCmsSerevice)
    }

}