package com.jms.fastor.feature.product.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.jms.fastor.feature.product.data.local.DbProduct.Companion.TABLE_NAME
import com.squareup.moshi.JsonClass


/**
 * Data class for Database entity and Serialization.
 */
@Entity(tableName = TABLE_NAME)
data class DbProduct(

    @PrimaryKey
    var id: String,
    var companyName: String? = null,
    var imageURL: String? = null,
    var names: List<ProductName>? = null,
    var prices: List<ProductPrice>? = null

) {
    companion object {
        const val TABLE_NAME = "products"
    }

}
@JsonClass(generateAdapter = true)
data class ProductName(
    var locale: String?=null,
    val name: String,
    val content: String? = null
)

@JsonClass(generateAdapter = true)
data class ProductPrice(
    val id: String,
    val price: String,
    val priceDesc: String?=null,
    val order: Int
)
