package com.jms.fastor.feature.product.data.remote.response


data class GetPagedProductsResponse(
    val currentPageNumber: Int,
    val lastPageNumber: Int,
    val originalSize: Int,
    val values: List<ProductResponse>?
)
data class ProductResponse(
    val id: String,
    val companyName: String?=null,
    val imageURL: String? = null,
    val nameList:List<ProductNameResponse>?,
    val priceListCms:List<ProductPriceResponse>?
)

data class ProductNameResponse(
    val locale: String,
    val name: String,
    val content: String? = null
)

data class ProductPriceResponse(
    val id: String,
    val price: String,
    val priceDesc: String?=null,
    val order: Int
)